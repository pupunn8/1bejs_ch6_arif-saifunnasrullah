const express = require('express');
const router = express.Router();
const { getAllUserBiodata, getBiodataById, createBiodata, updateBiodataById, deleteBiodataById } = require('../controller/user_biodata.controller');
const upload = require('../misc/multer');
const uploadCloudinary = require("../misc/cloudinary");

router.get('/', getAllUserBiodata);
router.get('/:id', getBiodataById);
router.post('/', upload.single('profile_picture'), uploadCloudinary, createBiodata);
router.post('/:id',upload.single('profile_picture'), uploadCloudinary,  updateBiodataById);
router.delete('/:id', deleteBiodataById);

module.exports = router;