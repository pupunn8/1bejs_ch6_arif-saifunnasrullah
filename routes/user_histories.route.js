const express = require('express');
const router = express.Router();
const { getAllUserHistories, getHistoryById, createHistory, updateHistoryById, deleteHistoryById } = require('../controller/user_histories.controller');
const upload = require('../misc/multer');
const uploadCloudinary = require("../misc/cloudinary");

router.get('/', getAllUserHistories);
router.get('/:id', getHistoryById);
router.post('/', upload.single('replay'), uploadCloudinary,  createHistory);
router.post('/:id',upload.single('replay'), uploadCloudinary,  updateHistoryById);
router.delete('/:id', deleteHistoryById);

module.exports = router;