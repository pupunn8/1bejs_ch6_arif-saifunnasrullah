const express = require('express');
const router = express.Router();
const { getAllUsers, getUserById, createUser, updateUserById, deleteUserById } = require('../controller/users.controller');

router.get('/', getAllUsers);
router.get('/:id', getUserById);
router.post('/', createUser);
router.post('/:id', updateUserById);
router.delete('/:id', deleteUserById);

module.exports = router;