const express = require('express');
const router = express.Router();
const {authenticateJWT} = require("../misc/middleware");
const userRoutes = require('./user.route');
const userBiodataRoutes = require('./user_biodata.route');
const userHistoriesRoutes = require('./user_histories.route');
const swaggerJSON = require('../api-documentation/swagger.json');
const swaggerUI = require('swagger-ui-express');
const { register, login, forgetPassword, resetPassword } = require('../controller/auth.controller');
const { createValidationFor, checkValidationResult } = require('../misc/validator');

// Homepage
router.get('/', (req, res) => {
    res.status(200).json({
        status: "Success",
        message: "Hello World!!!"
    });
});

// Documentation
router.use('/docs', swaggerUI.serve, swaggerUI.setup(swaggerJSON));

// Authentication
router.post('/register',  createValidationFor("register"), checkValidationResult, register);
router.post('/login', login);
router.post('/forget-password', forgetPassword);
router.post('/reset-password', resetPassword);

// Authentication Middleware
router.use(authenticateJWT);

router.use('/users', userRoutes);
router.use('/userBiodata', userBiodataRoutes);
router.use('/histories', userHistoriesRoutes);

module.exports = router;