'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_histories extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
        User_histories.belongsTo(models.User, {
            foreignKey: "user_id"
        });
      // define association here
    }
  }
  User_histories.init({
    user_id: DataTypes.INTEGER,
    is_win: DataTypes.BOOLEAN,
    enemy_id: DataTypes.INTEGER,
    total_turn: DataTypes.INTEGER,
    replay: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User_histories',
  });
  return User_histories;
};