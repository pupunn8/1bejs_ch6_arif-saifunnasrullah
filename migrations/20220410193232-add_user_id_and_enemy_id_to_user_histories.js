'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.addColumn(
            'User_histories',
            'user_id', {
            type: Sequelize.INTEGER,
            references: {
                model: 'Users',
                key: 'id'
            },
            onDelete: 'SET NULL',
            onUpdate: 'CASCADE'
        }
        )

        await queryInterface.addColumn(
            'User_histories',
            'enemy_id', {
            type: Sequelize.INTEGER,
            references: {
                model: 'Users',
                key: 'id'
            },
            onDelete: 'SET NULL',
            onUpdate: 'CASCADE'
        }
        )
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeColumn(
            'User_biodata',
            'user_id'
        )
        await queryInterface.removeColumn(
            'User_biodata',
            'enemy_id'
        )
    }
};
