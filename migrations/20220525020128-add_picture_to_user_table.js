'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn(
        'Users',
        'role', {
            type: Sequelize.STRING,
            default: "user"
        }
    );
    await queryInterface.addColumn(
        'User_biodata',
        'profile_picture', {
            type: Sequelize.STRING,
            allowNull: true
        }
    );
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeColumn(
        'Users',
        'role'
    )
    await queryInterface.removeColumn(
        'User_biodata',
        'profile_picture'
    )
  }
};
