const { User, User_biodata } = require("../models")
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const emailTransporter = require('../misc/mailer');
const {body, validationResult} = require("express-validator");

const register = async (req, res) => {
    try {
        const { email, password, player_name } = req.body;
        const foundEmail = await User.findOne({where: {email:email}});
        if(foundEmail) return res.status(401).json({status: "Error", message: "Email already registered"});
        
        const hashPassword = bcrypt.hashSync(password, +process.env.SALT_ROUNDS);
        const registeredUser = await User.create({
            email: email,
            password: hashPassword,
            role : "user"
        });
        const addUserBiodata = await User_biodata.create({
            user_id: registeredUser.id,
            player_name: player_name,
            level: 1
        });
    
        const userDetail = await User.findOne({
            where: {
                id: registeredUser.id
            },
            include: [User_biodata]
            }
        );

        const subject = "Welcome to Yugioh Games";
        const message = `
            <h2>Halo, ${player_name}</h2>
            <h3>Selamat Datang di Yugioh Games</h3>
            <p>Terimakasih telah mendaftar di game kami</p>
        `;
        const emailResponse = await emailTransporter(email, subject, message);

        res.status(201).json({
            status: "Success",
            message: `Registered successfully and Email successfully sent to ${email}`,
            data: userDetail
        });
    } catch (error) {
        console.log(error)
    }
}

const login = async (req, res) => {
    const { email, password } = req.body;
    const user = await User.findOne({
        where: {
            email: email
        },
        include: [User_biodata]
    });
    const isValidPassword = bcrypt.compareSync(password, user.password);
    if (isValidPassword) {
        const payload = {
            id: user.id,
            email: user.email,
            role: user.role,
            player_name: user.User_biodatum.player_name
        };
        const token = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET);
        return res.status(200).json({
            token: token
        });
    } else {
        return res.status(400).json({
            status: "Error",
            message: "Email or password incorrect"
        });
    }
}

const forgetPassword = async (req, res) => {
    try {
        // GET from body
        const { email } = req.body;

        //Create OTP
        const otp = Math.random().toString(36).slice(-6).toUpperCase();

        // Hash generated otp
        const hashedOtp = bcrypt.hashSync(otp, +process.env.SALT_ROUNDS);

        const updatedUser = await User.update({
            token: hashedOtp,
        }, {
            where: {
                email: email
            }
        });

        if (!updatedUser[0]) throw { message: 'Email is not registered', status: "Failed", code: 404 };

        const subject = "Password Reset";
        const message = `
            <h3>Anda mengirimkan permintaan reset password</h3>
            <p>OTP anda <b>${otp}</b>, gunakan otp tersebut sebagai kredensial untuk melakukan reset password</p>
        `;
        const emailResponse = await emailTransporter(email, subject, message);

        return res.status(200).json({
            message: `OTP successfully sent to ${email}`
        });
    } catch (error) {
        if (error.code) {
            return res.status(error.code).json({
                status: error.status,
                message: error.message
            });
        }
    }
}

const resetPassword = async (req, res) => {
    try {
        // Retrieve body
        const { email, newPassword, newPasswordConfirmation, otp } = req.body;

        if(newPassword === newPasswordConfirmation) {
            //Saerch user
            const foundUser = await User.findOne({where: {email:email}});

            // Check if user exist
            if (foundUser.token == null) throw { message: 'Must request forget password first', status: "Failed", code: 404 };
            if (!foundUser) throw { message: 'Email is not registered', status: "Failed", code: 404 };

            const compareOtp = bcrypt.compareSync(otp, foundUser.token);

            if(compareOtp) {
                await foundUser.update({
                    password: bcrypt.hashSync(newPassword, +process.env.SALT_ROUNDS),
                    token: null
                });

                return res.status(200).json({
                    status: "Success",
                    message: "Password sucessfully changed"
                });
            }

            throw { message: "Wrong OTP", status: "Failed", code: 400 };
        }

        throw { message: "Password doesn't match", status: "Failed", code: 400 };
    } catch (error) {
        if (error.code) {
            return res.status(error.code).json({
                status: error.status,
                message: error.message
            });
        }
    }
}

module.exports = {
    register,
    login,
    forgetPassword,
    resetPassword
}