const {User, User_biodata, User_histories} = require("../models");

const getAllUserHistories = async (req, res) => {
    try {
        let {page, row} = req.query;

        pages = ((page - 1) * row);

        let options;
        if(req.user.role === "user") {
            options = {
                where: {
                    user_id: req.user.id
                },
                attributes: ['id', 'user_id', 'is_win', 'enemy_id', 'total_turn', 'replay'],
                include: [User]
            };
        } else {
            options = {
                attributes: ['id', 'user_id', 'is_win', 'enemy_id', 'total_turn', 'replay'],
                include: [User]
            };
            
        }

        if (page && row) {
            options.offset = pages;
            options.limit = row;
        }

        const data = await User_histories.findAll(options);
        

        res.status(200).json({
            status: "Success",
            data: data
        });
    } catch (error) {
        res.status(400).json({
            status: "Error",
            message: error
        });
    }
}

const getHistoryById = async (req, res) => {
    try {
        let id = req.params.id;
        if(!( await User_histories.findByPk(id))) return res.status(404).json({status:"Error",message:"History not found!"});
        let history;
        if(req.user.role === "user") {
            history = await User_histories.findOne({
                where: {
                    id: id,
                    user_id: req.user.id
                }, include: [User, {model: User, include: [User_biodata]}]
            });
        } else {
            history = await User_histories.findOne({
                where: {
                    id: id
                }, include: [User, {model: User, include: [User_biodata]}]
            });
        }
        
        res.status(200).json({
            status: "Success",
            data: history
        });
    } catch (error) {
        res.status(404).json({
            status: "Error",
            message: "History not found!",
            error: error
        });
    }
}

const createHistory = async (req, res) => {
    try {
        if (req.user.role !== 'administrator') {
            return res.status(403).json({
                status: "Forbidden",
                message: "Anda tidak berkenan mengakses link ini"
            });
        }
        const {user_id, is_win, enemy_id, total_turn, url} = req.body;

        const createdHistory = await User_histories.create({
            user_id: user_id,
            is_win: is_win,
            enemy_id: enemy_id,
            total_turn: total_turn,
            replay: url
        });

        res.status(201).json({
            status: "Success",
            message: "Data created succesfully",
            data: createdHistory
        })
    } catch (error) {
        res.status(401).json({
            status: "Error",
            message: "Create data failed!",
            error: error
        });
    }
}

const updateHistoryById = async (req, res) => {
    try {
        if (req.user.role !== 'administrator') {
            return res.status(403).json({
                status: "Forbidden",
                message: "Anda tidak berkenan mengakses link ini"
            });
        }
        const {user_id, is_win, enemy_id, total_turn, url} = req.body;
        let id = req.params.id;
        if(!( await User_histories.findByPk(id))) return res.status(404).json({status:"Error",message:"History not found!"});

        const updatedHistory = await User_histories.update({
            user_id: user_id,
            is_win: is_win,
            enemy_id: enemy_id,
            total_turn: total_turn,
            replay: url
        }, {
            where: {
                id: id
            }, 
            returning: true
        });

        res.status(201).json({
            status: "Success",
            message: "Data updated successfully",
            data: updatedHistory[1]
        });
    } catch (error) {
        res.status(400).json({
            status: "Error",
            message: "Update data failed!",
            error: error
        });
    }
};

const deleteHistoryById = async (req, res) => {
    try {
        if (req.user.role !== 'administrator') {
            return res.status(403).json({
                status: "Forbidden",
                message: "Anda tidak berkenan mengakses link ini"
            });
        }
        let id = req.params.id;
        if(!( await User_histories.findByPk(id))) return res.status(404).json({status:"Error",message:"History not found!"});

        const deletedHistory = await User_histories.destroy({
            where: {
                id: id
            },
        });

        res.status(200).json({
            status: "Success",
            message: "Data deleted successfully",
            data: `History dengan id = ${id} berhasil terhapus`
        });
    } catch (error) {
        res.status(400).json({
            status: "Error",
            message: "Delete data failed!",
            error: error
        });
    }

};

module.exports = {
    getAllUserHistories,
    getHistoryById,
    createHistory,
    updateHistoryById,
    deleteHistoryById
};