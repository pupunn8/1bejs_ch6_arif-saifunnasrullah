require('dotenv').config();
const { User, User_biodata, User_histories } = require("../models");
const bcrypt = require("bcrypt");

const getAllUsers = async (req, res) => {
    try {
        const { role } = req.user;
    
        if (role !== 'administrator') {
            return res.status(403).json({
                status: "Forbidden",
                message: "Anda tidak berkenan mengakses link ini"
            });
        }
        const options = {
            attributes: ['id', 'email', 'password'],
        };

        if(req.query) {
            let { page, row } = req.query;
    
            let pages = ((page - 1) * row);
    
    
            if (page && row) {
                options.offset = pages;
                options.limit = row;
            }
        }

        const allUsers = await User.findAll(options);

        res.status(200).json({
            status: "Success",
            data: allUsers
        });
    } catch (error) {
        return res.status(500).json({ error: error.message })
    }
}

const getUserById = async (req, res) => {
    try {
        const { role } = req.user;
    
        if (role !== 'administrator') {
            return res.status(403).json({
                status: "Forbidden",
                message: "Anda tidak berkenan mengakses link ini"
            });
        }
        let id = req.params.id;
        if (!(await User.findByPk(id))) return res.status(404).json({ status: "Error", message: "User not found!" });
        const userDetail = await User.findOne({
            where: {
                id: id
            },
            include: [User_biodata, User_histories]
        }
        );

        res.status(200).json({
            status: "Success",
            data: userDetail
        });
    } catch (error) {
        res.status(404).json({
            status: "Error",
            message: "User not found!",
            error: error.message
        });
    }
}

const createUser = async (req, res) => {
    try {
        const { role } = req.user;
        console.log(req);
        if (role !== 'administrator') {
            return res.status(403).json({
                status: "Forbidden",
                message: "Anda tidak berkenan mengakses link ini"
            });
        }

        const { email, password } = req.body;
        const hashPassword = bcrypt.hashSync(password, +process.env.SALT_ROUNDS);
        const createdUser = await User.create({
            email: email,
            password: hashPassword,
            role: req.body.role
        });

        res.status(201).json({
            status: "Success",
            message: "Data created succesfully",
            data: createdUser
        })
    } catch (error) {
        res.status(401).json({
            status: "Error",
            message: "Create data failed!",
            error: error.message
        });
    }
}

const updateUserById = async (req, res) => {
    try {
        const { role } = req.user;
    
        if (role !== 'administrator') {
            return res.status(403).json({
                status: "Forbidden",
                message: "Anda tidak berkenan mengakses link ini"
            });
        }
        const { email, password } = req.body;
        console.log(req.body);
        const hashPassword = bcrypt.hashSync(password, +process.env.SALT_ROUNDS);
        let id = req.params.id;
        if (!(await User.findByPk(id))) return res.status(404).json({ status: "Error", message: "User not found!" });

        const updatedUser = await User.update({
            email: email,
            password: hashPassword,
            role: req.body.role
        }, {
            where: {
                id: id
            }, returning: true
        });

        res.status(201).json({
            status: "Success",
            message: "Data updated successfully",
            data: updatedUser[1]
        });
    } catch (error) {
        res.status(400).json({
            status: "Error",
            message: "Update data failed!",
            error: error.message
        });
    }
};

const deleteUserById = async (req, res) => {
    const { role } = req.user;

    if (role !== 'administrator') {
        return res.status(403).json({
            status: "Forbidden",
            message: "Anda tidak berkenan mengakses link ini"
        });
    }

    try {
        let id = req.params.id;
        if (!(await User.findByPk(id))) return res.status(404).json({ status: "Error", message: "User not found!" });

        const deletedUser = await User.destroy({
            where: {
                id: id
            }
        });

        res.status(200).json({
            status: "Success",
            message: "Data deleted successfully",
            data: `User dengan id = ${id} berhasil terhapus`
        });
    } catch (error) {
        res.status(400).json({
            status: "Error",
            message: "Delete data failed!",
            error: error.message
        });
    }

};

module.exports = {
    getAllUsers,
    getUserById,
    createUser,
    updateUserById,
    deleteUserById
};