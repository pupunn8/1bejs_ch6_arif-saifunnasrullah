const {User, User_biodata} = require("../models");

const getAllUserBiodata = async (req, res) => {
    try {
        let {page, row} = req.query;

        pages = ((page - 1) * row);

        let options;
        if(req.user.role === "user") {
            options = {
                where: {
                    user_id: req.user.id
                },
                attributes: ['id', 'user_id' ,'player_name', 'level', 'profile_picture'],
            };
        } else {
            options = {
                attributes: ['id', 'user_id' ,'player_name', 'level', 'profile_picture'],
            };
            
        }
        
        if (page && row) {
            options.offset = pages;
            options.limit = row;
        }

        const data = await User_biodata.findAll(options);

        res.status(200).json({
            status: "Success",
            data: data
        });
    } catch (error) {
        console.log(error);
    }
}

const getBiodataById = async (req, res) => {
    try {
        let id = req.params.id;
        if(!( await User_biodata.findByPk(id))) return res.status(404).json({status:"Error",message:"Biodata not found!"});
        let biodata;
        if(req.user.role === "user") {
            biodata = await User_biodata.findOne({
                where: {
                    id: id,
                    user_id: req.user.id
                },
                include: [User]
            });
        } else {
            biodata = await User_biodata.findOne({
                where: {
                    id: id
                },
                include: [User]
            });
        }
        
        res.status(200).json({
            status: "Success",
            data: biodata
        });
    } catch (error) {
        res.status(404).json({
            status: "Error",
            message: "Biodata not found!",
            error: error
        });
    }
}

const createBiodata = async (req, res) => {
    try {
        if (req.user.role !== 'administrator') {
            return res.status(403).json({
                status: "Forbidden",
                message: "Anda tidak berkenan mengakses link ini"
            });
        }

        const {user_id, player_name, level, url} = req.body;

        const createdBiodata = await User_biodata.create({
            user_id: user_id,
            player_name: player_name,
            level: level,
            profile_picture: url
        });

        res.status(201).json({
            status: "Success",
            message: "Data created succesfully",
            data: createdBiodata
        })
    } catch (error) {
        res.status(401).json({
            status: "Error",
            message: "Create data failed!",
            error: error
        });
    }
}

const updateBiodataById = async (req, res) => {
    try {
        const {player_name, level, url} = req.body;
        let id = req.params.id;
        if(!( await User_biodata.findByPk(id))) return res.status(404).json({status:"Error",message:"Biodata not found!"});
        
        const updatedBiodata = await User_biodata.update({
            player_name: player_name,
            level: level,
            profile_picture: url
        }, {
            where: {
                id: id
            }, returning: true
        });

        res.status(201).json({
            status: "Success",
            message: "Data updated successfully",
            data: updatedBiodata[1]
        });
    } catch (error) {
        res.status(400).json({
            status: "Error",
            message: "Update data failed!",
            error: error
        });
    }
};

const deleteBiodataById = async (req, res) => {
    try {
        if (req.user.role !== 'administrator') {
            return res.status(403).json({
                status: "Forbidden",
                message: "Anda tidak berkenan mengakses link ini"
            });
        }
        let id = req.params.id;
        if(!( await User_biodata.findByPk(id))) return res.status(404).json({status:"Error",message:"Biodata not found!"});

        const deletedBiodata = await User_biodata.destroy({
            where: {
                id: id
            }
        });

        res.status(200).json({
            status: "Success",
            message: "Data deleted successfully",
            data: `Biodata dengan id = ${id} berhasil terhapus`
        });
    } catch (error) {
        res.status(400).json({
            status: "Error",
            message: "Delete data failed!",
            error: error
        });
    }
};

module.exports = {
    getAllUserBiodata,
    getBiodataById,
    createBiodata,
    updateBiodataById,
    deleteBiodataById
};