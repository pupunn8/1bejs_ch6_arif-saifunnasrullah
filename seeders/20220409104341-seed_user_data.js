'use strict';

const usersData = require('../masterdata/users.json');
const bcrypt = require("bcrypt");

module.exports = {
  async up (queryInterface, Sequelize) {
    const users = usersData.map((eachUser) => {
        eachUser.password  = bcrypt.hashSync(eachUser.password, +process.env.SALT_ROUNDS);
        eachUser.createdAt = new Date();
        eachUser.updatedAt = new Date();
        return eachUser;
    })

    await queryInterface.bulkInsert('Users', users);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Users', null);
  }
};
