const nodemailer = require('nodemailer');

const sendEmail = async (receiver, subject, message) => {
    const transport = nodemailer.createTransport({
        host: "in-v3.mailjet.com",
        port: 587,
        secure: false,
        auth: {
            user: "8e7d0448430737ed174ba35f8e8711ff",
            pass: "04fcfcbe67aabdc5e63c5c70a7381052"
        }
    });
    const mail = {
        from: 'pupunn@students.unnes.ac.id',
        to: `${receiver}`,
        subject: `${subject}`,
        html: `${message}`
    };

    try {
        const info = await transport.sendMail(mail);
        console.log(info);
      } catch (err) {
        console.error(err);
      }
}

module.exports = sendEmail;