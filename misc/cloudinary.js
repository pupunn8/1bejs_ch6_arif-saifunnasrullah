const cloudinary = require("cloudinary").v2;
const fs = require("fs");

cloudinary.config({ 
    cloud_name: 'dohfbymmv', 
    api_key: '814843756465876', 
    api_secret: 'bWOVaw-6KO4ABxR-QFRbjNkQr7A' 
});

const uploadCloudinary = async (req, res, next) => {
    try {
        const folder = `assets/${req.file.mimetype.split('/')[0]}`;
        const uploadResult = await cloudinary.uploader.upload(req.file.path, {
            folder: folder,
            resource_type: "auto"
        });
        fs.unlinkSync(req.file.path);
        req.body.url = uploadResult.secure_url;
        next();
    } catch (error) {
        fs.unlinkSync(req.file.path);
        console.log(error);
    }
};

module.exports = uploadCloudinary;